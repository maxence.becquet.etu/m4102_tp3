package fr.ulille.iut.pizzaland.beans;

import java.util.List;

import fr.ulille.iut.pizzaland.dto.IngredientCreateDto;
import fr.ulille.iut.pizzaland.dto.IngredientDto;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

public class Pizza {
	
	private long id;
	private String name;
	private List<Ingredient> lesIngredients;

	public long getId() {
		return this.id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public List<Ingredient> getLesIngredients() {
		return this.lesIngredients;
	}
	
	public void setIngredients(List<Ingredient> lesIngredients) {
		this.lesIngredients = lesIngredients;
	}

	public static Pizza fromDto(PizzaDto dto) {
		Pizza pizza = new Pizza();
		pizza.setId(dto.getId());
		pizza.setName(dto.getName());
		pizza.setIngredients(dto.getLesIngredients());
		return pizza;
	}

	public static PizzaDto toDto(Pizza p) {
		PizzaDto dto = new PizzaDto();
		dto.setId(p.getId());
		dto.setName(p.getName());
		dto.setIngredients(p.getLesIngredients());
		return dto;
	}

	public static PizzaCreateDto toCreateDto(Pizza pizza) {
		PizzaCreateDto dto = new PizzaCreateDto();
		dto.setName(pizza.getName());
		return dto;
	}

	public static Pizza fromPizzaCreateDto(PizzaCreateDto dto) {
		Pizza pizza = new Pizza();
		pizza.setName(dto.getName());
		return pizza;
	}
	
	public String ingredientsToString() {
		String res = "";
		for(Ingredient i : this.lesIngredients) {
			res += i.toString() + " ";
		}
		return res;
	}
	
	@Override
	public String toString() {
		return "Pizza [id=" + id + ", lesIngredients=" + this.ingredientsToString() + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pizza other = (Pizza) obj;
		if (id != other.id)
			return false;
		if (lesIngredients == null) {
			if (other.lesIngredients != null)
				return false;
		} else if (!lesIngredients.equals(other.lesIngredients))
			return false;
		return true;
	}
}
