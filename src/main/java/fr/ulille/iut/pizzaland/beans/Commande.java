package fr.ulille.iut.pizzaland.beans;

import java.util.List;

import fr.ulille.iut.pizzaland.dto.CommandeCreateDto;
import fr.ulille.iut.pizzaland.dto.CommandeDto;

public class Commande {
	
	private long id;
	private String nom;
	private String prenom;
	private List<Pizza> lesPizzas;
	
	public Commande() {}
	
	public long getId() {
		return this.id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getNom() {
		return this.nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public List<Pizza> getLesPizzas() {
		return this.lesPizzas;
	}
	
	public void setIngredients(List<Pizza> lesPizzas) {
		this.lesPizzas = lesPizzas;
	}

	public static Commande fromDto(CommandeDto dto) {
		Commande commande = new Commande();
		commande.setId(dto.getId());
		commande.setNom(dto.getNom());
		commande.setPrenom(dto.getPrenom());
		commande.setIngredients(dto.getLesPizzas());
		return commande;
	}

	public static CommandeDto toDto(Commande c) {
		CommandeDto dto = new CommandeDto();
		dto.setId(c.getId());
		dto.setNom(c.getNom());
		dto.setPrenom(c.getPrenom());
		dto.setIngredients(c.getLesPizzas());
		return dto;
	}

	public static CommandeCreateDto toCreateDto(Commande c) {
		CommandeCreateDto dto = new CommandeCreateDto();
		dto.setNom(c.getNom());
		dto.setPrenom(c.getPrenom());
		dto.setLesPizzas(c.getLesPizzas());
		return dto;
	}

	public static Commande fromCommandeCreateDto(CommandeCreateDto dto) {
		Commande commande = new Commande();
		commande.setNom(dto.getNom());
		commande.setPrenom(dto.getPrenom());
		commande.setIngredients(dto.getLesPizzas());
		return commande;
	}
	
	public String toString() {
		return "Commande[id="+this.id+", nom="+this.nom+", prenom="+this.prenom+", pizzas=";
	}
}
