package fr.ulille.iut.pizzaland.dto;

import java.util.List;

import fr.ulille.iut.pizzaland.beans.Pizza;

public class CommandeDto {

	private long id;
	private String nom;
	private String prenom;
	private List<Pizza> lesPizzas;

	public CommandeDto() {}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}


	public String getNom() {
		return this.nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public List<Pizza> getLesPizzas() {
		return this.lesPizzas;
	}

	public void setIngredients(List<Pizza> lesPizzas) {
		this.lesPizzas = lesPizzas;
	}
}
