package fr.ulille.iut.pizzaland.dto;

import java.util.List;

import fr.ulille.iut.pizzaland.beans.Pizza;

public class CommandeCreateDto {

	private String nom;
	private String prenom;
	private List<Pizza> lesPizzas;
	
	public CommandeCreateDto() {}
	
	public String getNom() {
		return this.nom;
	}
	
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public String getPrenom() {
		return this.prenom;
	}
	
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	public List<Pizza> getLesPizzas() {
		return this.lesPizzas;
	}
	
	public void setLesPizzas(List<Pizza> lesPizzas) {
		this.lesPizzas = lesPizzas;
	}
}
