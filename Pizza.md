## Développement d'une ressource *Pizza*

### API et représentation des données

Voici les URI correpondant à notre API Rest pour la ressource Pizza.

| Opération | URI         | Action réalisée                               | Retour                                        |
|:----------|:------------|:----------------------------------------------|:----------------------------------------------|
| GET       | /pizzas     | récupère l'ensemble des ingrédients           | 200 et un tableau de pizza              |
| GET       | /pizzas/{id}| récupère la pizza d'identifiant id            | 200 et la pizza                           |
|           |             |                                               | 404 si id est inconnu                         |
| GET       | /pizzas/{id}/name | récupère le nom de la pizza             | 200 et le nom de la pizza               |
|           |             | d'identifiant id                              | 404 si id est inconnu                         |
| POST      | /pizzas | création d'une pizza                              | 201 et l'URI de la ressource créée + représentation |
|           |             |                                               | 400 si les informations ne sont pas correctes |
|           |             |                                               | 409 si la pizza existe déjà (même nom)    |
| DELETE    | /pizzas/{id}| destruction de la pizza d'identifiant id      | 204 si l'opération à réussi                   |
|           |             |                                               | 404 si id est inconnu                         |


Une pizza comporte un identifiant, un nom et sa liste d'ingrédients. Sa
représentation JSON prendra donc la forme suivante :

    {
	  "id": 1,
	  "name": "reine",
	  "ingredients":
	  					[
	  					 { "name": "jambon"},
	  					 {	"name": "olives"}
	  					 ]
	}
	
Lors de la création, l'identifiant n'est pas connu car il sera fourni
par la base de données. Aussi on aura une
représentation JSON qui comporte uniquement le nom :

	{ "name": "mozzarella", "ingredients": [{"name": "jambon"},{"name": "olives"}] }
