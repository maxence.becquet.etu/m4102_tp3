## Développement d'une ressource *Commande*

### API et représentation des données

Voici les URI correpondant à notre API Rest pour la ressource Commande.

| Opération | URI         | Action réalisée                               | Retour                                        |
|:----------|:------------|:----------------------------------------------|:----------------------------------------------|
| GET       | /commandes | récupère l'ensemble des commandes          | 200 et un tableau de commandes               |
| GET       | /commandes/{id} | récupère la commande d'identifiant id  | 200 et la commande                           |
|           |             |                                               | 404 si id est inconnu                         |
| GET       | /commandes/{id}/name | récupère le nom de la commande    | 200 et le nom de la commande                 |
|           |             | d'identifiant id                              | 404 si id est inconnu                         |
| POST      | /commandes | création d'une commande                     | 201 et l'URI de la ressource créée + représentation |
|           |             |                                               | 400 si les informations ne sont pas correctes |
|           |             |                                               | 409 si la commande existe déjà (même nom)    |
| DELETE    | /commandes/{id} | destruction de la commande d'identifiant id | 204 si l'opération à réussi                   |
|           |             |                                               | 404 si id est inconnu                         |


Une Commande comporte un identifiant, un nom, un prénom et une liste de pizzas. Sa
représentation JSON prendra donc la forme suivante :

    {
	  "id": 1,
	  "name": "BECQUET",
	  "prenom": "Maxence",
	  "lesPizzas": [
	  			{"name": "reine"},
	  			{"name": "cannibale"}
	  ]
	}
	
Lors de la création, l'identifiant n'est pas connu car il sera fourni
par la base de données. Aussi on aura une
représentation JSON qui comporte uniquement le nom :

	{ "name": "BECQUET", "prenom": "Maxence", "lesPizzas": [{"name": "reine"}, {"name": "cannibale"}] }
